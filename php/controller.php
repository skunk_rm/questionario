<?php
require 'config.inc.php';
require 'DB.php';

session_start();

//controllo le chimate fatte da JQuery
if(isset($_POST['source'])){
    if(isset($_POST['source'])){
        $source = $_POST['source'];
        switch ($source) {
            case 'salvaSection':
                salvaSezione();
                break;
            case 'inviaSondaggio':
                inviaSondaggio();
                break;
            case 'checkSession':
                checkSession();
        }
    }
}


function salvaQuestionario($q1, $q2, $q3, $q4, $q5, $q6, $q7, $q8, $q9, $q10, $media, $centro){
    $DBa = DB::getInstance();
    $DBa->insert('esitoquestionario', [
        'idStruttura' => $centro,
        'dataValutazione' => date('Y-m-d H:i:s'),
        'q1'	=> $q1,
        'q2'	=> $q2,
        'q3'	=> $q3,
        'q4'	=> $q4,
        'q5'	=> $q5,
        'q6'	=> $q6,
        'q7'	=> $q7,
        'q8'	=> $q8,
        'q9'	=> $q9,
        'q10'	=> $q10,
        'media' => $media
    ]);

    
}

function salvaSezione(){
    if(isset($_POST['sezione'])){
        $_SESSION['sezione'.$_POST['sezione']] = $_POST['voto'];
    }
}

function inviaSondaggio(){
    for($i=1;$i<11;$i++){
        if(isset($_SESSION['sezione'.$i]) & $_SESSION['sezione'.$i]!=""){
            $_SESSION['media'] = ($_SESSION['media'] + $_SESSION['sezione'.$i])/$i;
        }
    }
    
    resetSession();
}

function resetSession(){
    for($i=1;$i<11;$i++){
        if(isset($_SESSION['sezione'.$i])){
            $_SESSION['sezione'.$i] = "";
        }
    }
    
    $_SESSION['media'] = "";
    $_SESSION['timeout'] = "";
    $_SESSION['oldSession']="";
    $_SESSION['currentSession']="";
}


function checkSession(){
    
    if(isset($_SESSION['oldSession'])){
        if($_SESSION['oldSession']!=""){
            if($_SESSION['currentSection']==$_SESSION['oldSession']) {
                
            } else {
                $_SESSION['oldSession'] = $_SESSION['currentSection'];
                unset($_SESSION['timeout']);
            }
        } else {
            $_SESSION['oldSession'] = $_SESSION['currentSection'];
            unset($_SESSION['timeout']);
        }
    } else {
        $_SESSION['oldSession'] = $_SESSION['currentSection'];
        unset($_SESSION['timeout']);
    }
    
    // set timeout period in seconds
    $inactive = 49;
    $session_life = 0;
    
    // check to see if $_SESSION['timeout'] is set
    if(isset($_SESSION['timeout']) ) {
        $session_life = time() - $_SESSION['timeout'];
        if($session_life > $inactive){
            session_destroy();
            resetSession();
            $session_life = "KO";
        }
    } else {
        $_SESSION['timeout'] = time();
    }
    echo $session_life;
}

?>